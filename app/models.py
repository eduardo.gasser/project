# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Model(models.Model):
    foo = models.BooleanField(default=False)
    bar = models.CharField(max_length=200)
    plus = models.CharField(max_length=200, null=True)

    def __unicode__(self):
        return u"%s %s" % (self.foo, self.bar)
