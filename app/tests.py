# -*- coding: utf-8 -*-
from django.test import TestCase

from app.models import Model

class ModelTestCase(TestCase):
    def setUp(self):
        Model.objects.create(bar="lion", foo=True)
        Model.objects.create(bar="cat", foo=False)

    def test_model(self):
        lion = Model.objects.get(bar="lion")
        cat = Model.objects.get(bar="cat")
        self.assertEqual(lion.bar, 'lion')
        
    def test_model_2(self):
        
        self.assertEqual(Model.objects.count(), 2)
