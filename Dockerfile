FROM phusion/baseimage:latest

# Setup enviroment
RUN mkdir -p /root/home/build
RUN apt-get update
RUN apt-get install -y python-pip
ADD build/req.txt /root/home/build/req.txt
RUN pip install -r /root/home/build/req.txt

EXPOSE 8000

# END Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
