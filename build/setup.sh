#!/bin/sh

# Setup database
/sbin/setuser postgres /etc/init.d/postgresql start;
/sbin/setuser postgres psql -c "create database test_project;"
/sbin/setuser postgres psql -c "create user test_project password 'test_project'"
/sbin/setuser postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE test_project TO test_project;"
/sbin/setuser postgres psql -d test_project -f /var/lib/postgresql/schema.sql

# Init deamons
/sbin/my_init
